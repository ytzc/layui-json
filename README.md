# layui-json

### 介绍
layui-json是layui的第三方扩展，layui-json用json形式构建layui模块，json在过去常用于写配置文件，layui-json延续了json写配置简单易理解的优势，通过json写配置构建layui后台管理界面。 
### 返虚归真
layui-json构建的界面，延续了layui返虚归真，从使用上尽可能的简单，同时又具有较强的交互性，例如同样可以完成表单提交、提交成功后的事件操作：刷新页面、关闭窗口、打开窗口、搜索列表等后台管理系统中必备的功能。 
### 优势
layui-json的优势在于不用写HTML、CSS、JS就可以构建后台管理界面，有别于传统的开发方式，layui-json使用了一种优雅渐进性的后台构建理念。使用layui-json构建后台，你完全可以不关注前端，仅关注业务逻辑即可，因为你无需写一个前端代码。
### 语法
layui-json的核心是json格式语法，语法本身不用安装，只需要编写成json文件存储在服务器中可以通过URL访问即可，需要安装的只是layui-json扩展，这也是layui-json的核心思想之一，使用者无需关注layui代码，只需要掌握json格式语法即可快速构建后台管理系统。
### 扩展
layui-json的理念是用json配置的形式调用layui的所有组件，大家在深入使用过程中可能需要用到一些layui本身还没有的组件，这时可以利用扩展进行二次开发或者集成其他UI组件，layui-json提供了一种极其便利的扩展方法如果layui扩展一样便利。

### 快速安装
- 安装layui-json非常简单，只需要正确的加载layui-json扩展即可。
- 先下载layui-json.js文件，将其放到你的网站静态目录中：
- 在站点项目下新建一个index.html文件，文件内容如下：

```
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="http://www.layuicdn.com/layui-v2.6.8/css/layui.css" media="all">
    <script type="text/javascript" src="http://www.layuicdn.com/layui-v2.6.8/layui.js"></script>
    <script type="text/javascript">
        //加载layui-jsonui扩展
        layui.extend({
            //layui-jsonui文件所在地址，忽略文件扩展名(.js)
            json: "{/}/static/layui-jsonui"
        });
        //使用json扩展
	layui.use(['json'], function () {
            //实例化窗口
            layui.json.parser.open.init({
                //传入json构建界面
                content: decodeURIComponent(jsonui.parser.tool.getQuery("content"))
            });
        });
    </script>
</head>

<body>
</body>
</html>
```
### 测试安装结果
- 在站点项目下创建一个`1.json`文件，内容如下：

```
{
    "code": 0,
    "msg": "ok",
    "result": {
        "module": {
            "print": "Hello world"
        }
    }
}
```
- 文件建好以后，访问：`http://你的域名/index.html?content=1.json`
- 如果能输出`Hello world`说明安装正确。


## 阅读文档

- [layui-json文档手册](https://gitee.com/yutangzongcai/layui-json/wikis)

## layui相关
- [layui官方gitee仓库](https://gitee.com/sentsin/layui)
- [layui镜像站](https://layui.jsonui.net)
- [layui文档](https://layui.jsonui.net/doc/index.html)